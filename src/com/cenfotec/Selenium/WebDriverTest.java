package com.cenfotec.Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


public class WebDriverTest {

    private final String BASE_URL = "http://demo.nopcommerce.com/";
    private WebDriver driver;
    private int browser;

    public WebDriverTest(int browser) {
        this.browser = browser;
    }

    /**
     * Método que prueba que el título del sitio sea igual al titulo esperado
     * en la variable expectedTitle.
     * Test Case 1
     */
    public void titleTest() throws InterruptedException {
        setBrowser();
        driver.get(BASE_URL);
        //Variable con el texto a ser evaluado
        String expectedTitle = "nopCommerce demo store";
        //Se obtiene el titulo de la página y se compara con el contenido de la variable title.
        String actualTitle = driver.getTitle();

        if ((actualTitle.contentEquals(expectedTitle))) {
            System.out.println("El titulo es correcto");
        } else {
            System.out.println("El titulo es incorrecto");
        }

        Thread.sleep(5000);
        driver.close();
    }

    /**
     * Método que se encarga de probar la navegabilidad del sitio haciendo click en los diferentes
     * enlaces de la barra de navegacion y esperando 3 segundos antes dar click al siguiente enlace.
     * Test Case 2
     */
    public void navigationTest(){
        setBrowser();
        int waitTime = 3;
        driver.get(BASE_URL);
        try {
            /*
             * Se visita la categoria de Computadoras y se espera 3 segundos.
             */
            driver.findElement(By.partialLinkText("omputers")).click();
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

            /*
             * Se visita la categoria de Electronics y se espera 3 segundos.
             */
            driver.findElement(By.partialLinkText("lectronics")).click();
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

            /*
             * Se visita la categoria de Apparel y se espera 3 segundos.
             */
            driver.findElement(By.partialLinkText("pparel")).click();
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

            /*
             * Se visita la categoria de downloads y se espera 3 segundos.
             */
            driver.findElement(By.partialLinkText("downloads")).click();
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

            /*
             * Se visita la categoria de books y se espera 3 segundos.
             */
            driver.findElement(By.partialLinkText("ooks")).click();
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

            /*
             * Se visita la categoria de Jewelry y se espera 3 segundos.
             */
            driver.findElement(By.partialLinkText("ewelry")).click();
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

            /*
             * Se visita la categoria de Gift Cards y se espera 3 segundos.
             */
            driver.findElement(By.partialLinkText("ift")).click();
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

            /*
             * Se regresa al homepage dando click en el logo de la esquina superior derecha,
             * ademas se escribe en consola que la prueba termino con exito.
             */
            driver.findElement(By.className("header-logo")).click();
            System.out.println("La prueba corrio de manera exitosa");

        } catch (NoSuchElementException exception) {

            /*
             * En caso que uno de los elemento no se encuentra dentro de la busqueda en el DOM de la pagina,
             * se atrapa el error y se imprime en consola que la prueba fallo.
             */
            System.out.println("Error, uno de los links del header del website no fue encontrado durante la prueba.");
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Método que se encarga de probar las diferentes acciones para agregar un elemento al wishlist,
     * se comienza navegando al wishlist, se verifica que este vacía, se busca un elemento en la barra de búsqueda,
     * se agrega al wishlist y se verifica que se haya agregado correctamente.
     * Test Case 3
     */
    public void wishListTest() {
        setBrowser();
        //Variable que define el texto a verificar en el wishlist vacío
        String expectedEmptyText = "The wishlist is empty!";
        //Variable que define el texto a ser ingresado en la búsqueda
        String itemName = "Fahrenheit 451";
        //Se establece un tiempo de 5 segundos como máximo para esperar a que los elementos sean cargados
        WebDriverWait wait = new WebDriverWait(driver, 5);
        boolean emptyDisplayed, itemAdded;
        driver.get(BASE_URL);

        try {

            //Se obtiene el elemento para dirigirse al wishlist y se le da click
            driver.findElement(By.xpath("/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[3]/a")).click();

            //Se obtiene el texto que se muestra cuando el wishlist esta vacío y se compara su equivalencia con la variable wishListEmptyText
            WebElement actualText = driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div/div/div[2]/div"));
            //Se alamcena el resultado de la equivalencia en el boolean emptyDisplayed
            emptyDisplayed = expectedEmptyText.equals(actualText.getText());

            //Se obtiene el elemento de la barra de búsqueda y se ingresa el texto de la variable itemName
            driver.findElement(By.id("small-searchterms")).sendKeys(itemName);

            //Se obtiene el boton de busqueda y se le da click
            driver.findElement(By.xpath("//*[@id=\"small-search-box-form\"]/input[2]")).click();

            //Se espera a que el boton de agregar al wishilist aparezca, se obtiene el elemento y se le da click
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[6]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div[2]/div[3]/div[2]/input[3]"))).click();

            //Se espera a que la notificacion de elemento agregado al wishlist aparezca se obtiene el elemento para dirigirse al wishlist y se le da click
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"bar-notification\"]/div/p/a"))).click();

            //Se espera a que la tabla con los items de wishlist aparezca para poder ser evaluada
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table")));

            //Se obitene el nombre del elemento añadido al wishlist y se verifica que contenga el texto de la variable itemName
            WebElement wishListItemName = driver.findElement(By.xpath("/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/form/div[1]/table/tbody/tr/td[5]/a"));
            //Se alamecan el resultado de la funcion contains en el boolean itemAdded
            itemAdded = wishListItemName.getText().contains(itemName);

            //Se comparan los resultados de las equivalencias y se imprime un mensaje con el resultado de las pruebas
            if (emptyDisplayed && itemAdded) {
                System.out.println("La prueba corrio de manera exitosa");
            } else {
                System.out.println("Hubo un error en la prueba");
            }

        } catch (NoSuchElementException exception) {
            /*
             * En caso que uno de los elemento no se encuentra dentro de la busqueda en el DOM de la pagina,
             * se atrapa el error y se imprime en consola que la prueba fallo.
             */
            System.out.println("Error, uno de los links del header del website no fue encontrado durante la prueba.");
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Método para setear el navegador web deseado. 1 = Chrome, 2 = Firefox
     *
     */
    private void setBrowser() {
        switch (this.browser) {
            //Chrome
            case 1:
                System.setProperty("webdriver.chrome.driver", "C:\\SeleniumWebDrivers\\chromedriver.exe");
                driver = new ChromeDriver();
                break;
            //Firefox
            case 2:
                System.setProperty("webdriver.gecko.driver", "C:\\SeleniumWebDrivers\\geckodriver.exe");
                driver = new FirefoxDriver();
        }
    }

}
